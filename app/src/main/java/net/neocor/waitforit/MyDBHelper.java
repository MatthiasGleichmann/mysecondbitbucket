package net.neocor.waitforit;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class MyDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_TABLE_CREATE = "CREATE TABLE myentries( _id integer primary key, title text not null, enddate date not null);";
    private static final String DATABASE_TABLE_DROP = "DROP TABLE IF EXISTS myentries;";

    public MyDBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_TABLE_CREATE);
    }

    // Method is called during an upgrade of the database,
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL(DATABASE_TABLE_DROP);
        onCreate(database);
    }

    public void ExecSQL(String sql) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(sql);
    }

    public Cursor getCursor(String sql) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur =  db.rawQuery( sql, null );
        return cur;
    }

}