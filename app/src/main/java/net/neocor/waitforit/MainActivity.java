package net.neocor.waitforit;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import static android.provider.AlarmClock.EXTRA_MESSAGE;


public class MainActivity extends AppCompatActivity {

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static TimePoint CreateEntry(String title, String start, String end) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        ZoneId zone = ZoneId.systemDefault();

        TimePoint pe = new TimePoint();

        pe.Title = title;
        pe.PeriodHasStart = true;
        pe.start = LocalDateTime.parse(start, formatter).atZone(zone);
        pe.end = LocalDateTime.parse(end, formatter).atZone(zone);

        return pe;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {

            int rows = 0;

            MyDBHelper dbHelper = new MyDBHelper(this, "MyDatabase.DB", null, 3);

            dbHelper.ExecSQL("INSERT INTO myentries(title, enddate) VALUES ('aaa',DATE('now'))");

            Cursor c;

            c = dbHelper.getCursor("SELECT COUNT(*) AS CNT FROM myentries");
            c.moveToFirst();
            rows = c.getInt(0);
            c.close();

            final TimePoint[] mype = new TimePoint[rows]; //Obacht

            c = dbHelper.getCursor("SELECT _id,title,enddate FROM myentries");
            c.moveToFirst();
            int row = 0;
            while (c.isAfterLast() == false) {
                mype[row] = CreateEntry(c.getString(1), "1968-06-01 00:00:00", c.getString(2));
                row += 1;
                c.moveToNext();
            }
            c.close();


            LinearLayout linlay1 = (LinearLayout) findViewById(R.id.linlay1);
            final TextView textWatch = (TextView) findViewById(R.id.textWatch);

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            for (int i = 0; i < rows; i++) {

                View custom = inflater.inflate(R.layout.listentry, null);
                TextView tv;
                tv = (TextView) custom.findViewById(R.id.text1);
                tv.setText(mype[i].Title);
                tv = (TextView) custom.findViewById(R.id.text2);
                mype[i].text2 = tv;

                linlay1.addView(custom);
            }

            final Handler handler = new Handler();
            Timer timer = new Timer(false);

            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {


                            for (int i = 0; i < mype.length; i++) {

                                ZonedDateTime zd1 = ZonedDateTime.ofInstant(Calendar.getInstance().getTime().toInstant(), ZoneId.systemDefault());
                                ZonedDateTime zd2 = mype[i].end;

                                textWatch.setText(DateTimeFormatter.ofPattern("dd.MM.yyyy kk:mm:ss").format(zd1));

                                long d = ChronoUnit.DAYS.between(zd1, zd2);
                                long hours = ChronoUnit.HOURS.between(zd1, zd2) % 24;
                                long mins = ChronoUnit.MINUTES.between(zd1, zd2) % 60;
                                long secs = ChronoUnit.SECONDS.between(zd1, zd2) % 60;

                                DecimalFormat df1 = new DecimalFormat("00000");
                                DecimalFormat df2 = new DecimalFormat("00");

                                mype[i].text2.setText(df1.format(d) + ":" + df2.format(hours) + ":" + df2.format(mins) + ":" + df2.format((secs)));

                            }

                        }
                    });
                }
            };

            timer.schedule(timerTask, 1000, 1000); // 1000 = 1 second.

        } catch (Exception e) {
            System.out.println(e.toString());
        }

    }

    public void Entry_Click(View view) {

        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(EXTRA_MESSAGE, "one");
        startActivity(intent);

    }


}
